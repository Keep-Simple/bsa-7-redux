import React from 'react';
import ReactDOM from 'react-dom';
import Chat from "./container/Chat";
import "./container/common.css";
import {Provider} from "react-redux";
import store from "./container/redux/store";

ReactDOM.render(
    <Provider store={store}>
        <Chat/>
    </Provider>,
    document.getElementById('root')
);
