export type AppState = {
    messages: MessageState[],
    editWindow: MessageState
};

export interface MessageState {
    name: string;
    date: string;
    avatar: string;
    edit_date: string;
    id: string;
    text: string;
    user_id: string;
    isLiked: boolean;
}
