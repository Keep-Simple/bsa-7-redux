import React from 'react';
import {Comment} from "semantic-ui-react";
import moment from "moment";
import './styles.css';
import Message from "../Message/Message";
import {AppState, MessageState} from "../../types/types";
import {connect, ConnectedProps} from "react-redux";
import adminInfo from "../../mock/Admin";

const MessageList = (props: MessageListProps) => {

    const {messages} = props;
    let prevDate: Date;


    const insertDividerIfNeeded = (ms: MessageState) => {
        const mountDate = new Date(ms.date);

        if (prevDate?.getDay() !== mountDate.getDay()) {
            prevDate = mountDate;
            return (
                <div className="timestamp">
                    {moment(mountDate).format('LLLL')}
                </div>
            );
        }
        prevDate = mountDate;
    }

    const defineLastOwnMessage = () => {
        if (messages.length > 0) {
            return messages
                .filter(m => m.user_id === adminInfo.user_id)
                .slice(-1)[0];
        }
        return {};
    }

    return (
        <Comment.Group size="large" className="commentContainer">
            {props.messages.map(ms => (
                <span key={ms.id}>
                {insertDividerIfNeeded(ms)}
                    <Message ms={ms} isLast={ms === defineLastOwnMessage()}/>
                </span>))}
        </Comment.Group>

    );
}

const mapState = (state: AppState) => ({
    messages: state.messages
});

const connector = connect(mapState);

type MessageListProps = ConnectedProps<typeof connector>;

export default connector(MessageList);
