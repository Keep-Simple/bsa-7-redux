import React, {FormEvent, useState} from 'react';
import {Button, Form, Icon, Segment} from 'semantic-ui-react';
import './styles.css';
import {connect, ConnectedProps} from "react-redux";
import {sendMessage} from "../../container/redux/actionCreators";

const MessageInput = ({send}: MessageInputProps) => {
    const [body, setBody] = useState('');

    const handlePost = (e: KeyboardEvent | FormEvent) => {
        if (body.trim()) {
            e.preventDefault();
            send(body);
            setBody('');
        }
    };

    return (
        <Segment clearing className="editSegment">
            <Form onSubmit={handlePost}>
                <Form.TextArea
                    onKeyPress={(e: KeyboardEvent) => e.key === "Enter" && !e.shiftKey && handlePost(e)}
                    autoFocus
                    className="textArea"
                    value={body}
                    placeholder="What's happening?"
                    onChange={(ev: FormEvent) => setBody((ev.target as HTMLTextAreaElement).value)}
                />
                <Button
                    disabled={!body.length}
                    className="inputButton"
                    floated="right"
                    type="submit"
                    size="big"
                    animated="vertical"
                >
                    <Button.Content visible><b>Send</b></Button.Content>
                    <Button.Content hidden><Icon name="arrow up"/></Button.Content>
                </Button>
            </Form>
        </Segment>
    );
};

const mapDispatch = {
    send: sendMessage
}

const connector = connect(null, mapDispatch);

type MessageInputProps = ConnectedProps<typeof connector>;

export default connector(MessageInput);
