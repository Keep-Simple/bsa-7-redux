import React, {useState} from 'react';
import {Comment, Confirm, Icon, Popup} from "semantic-ui-react";
import moment from "moment";
import './styles.css';
import admin from '../../mock/Admin';
import {connect, ConnectedProps} from "react-redux";
import {deleteMessage, likeMessage, toggleEditWindow} from "../../container/redux/actionCreators";
import {MessageState} from "../../types/types";

type MessageProps = ReduxProps & { ms: MessageState, isLast: boolean }

const Message = (props: MessageProps) => {

    const {ms, deleteMs, toggleEdit, likeMs, isLast} = props;
    const [delOpen, setDelModel] = useState(false);
    const isAdmin = ms.user_id === admin.user_id;

    return (
        <>{isAdmin ?
            <Popup mouseLeaveDelay={600} position="right center" hoverable className="popupIcons"
                   trigger={
                <Comment className="bubble mine">
                    <Comment.Content>
                        <Comment.Author as="a" className="author">{ms.name}</Comment.Author>
                        <Comment.Metadata>
                            at {moment(new Date(ms.date)).format("LT")}
                        </Comment.Metadata>
                        <Comment.Text style={{marginTop: '7px'}}>
                            {ms.text}
                        </Comment.Text>
                    </Comment.Content>
                </Comment>}>
                <Popup.Content >
                        <Icon link name="recycle" onClick={() => setDelModel(true)}/>
                        {isLast && <Icon link name="edit outline" onClick={() => toggleEdit(ms)}/>}
                </Popup.Content>
            </Popup>
            :
            <Comment className="bubble">
                <Comment.Avatar className="avatar" src={ms.avatar}/>
                <Comment.Content>
                    <Comment.Author as="a" className="author">{ms.name}</Comment.Author>
                    <Comment.Metadata>
                        at {moment(new Date(ms.date)).format("LT")}
                    </Comment.Metadata>
                    <Comment.Text style={{marginTop: '7px'}}>
                        {ms.text}
                    </Comment.Text>
                    <Comment.Actions className="commonAction">
                        <Comment.Action onClick={() => likeMs(ms)}>
                            <Icon color={ms.isLiked ? 'red' : 'grey'} name='like'/>
                        </Comment.Action>
                    </Comment.Actions>
                </Comment.Content>
            </Comment>
        }
            <Confirm
                dimmer="blurring"
                className="deleteModal"
                confirmButton='Delete'
                content='Delete this message?'
                size='mini'
                open={delOpen}
                onCancel={() => setDelModel(false)}
                onConfirm={() => deleteMs(ms)}
            />
        </>
    );
}

const mapDispatch = {
    likeMs: likeMessage,
    deleteMs: deleteMessage,
    toggleEdit: toggleEditWindow
}

const connector = connect(null, mapDispatch);

type ReduxProps = ConnectedProps<typeof connector>;

export default connector(Message);
