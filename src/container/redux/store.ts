import {applyMiddleware, createStore,} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {rootReducer} from "./rootReducer";
import {mock} from "../../mock/MOCK_DATA";
import {MessageState} from "../../types/types";

const initialMessages = mock.sort((a: MessageState, b: MessageState) => (
    new Date(a.date).getTime() - new Date(b.date).getTime()));

const initialState = { messages: initialMessages, editWindow: {text: ''} as MessageState};

const composedEnhancers = composeWithDevTools(applyMiddleware());

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);

export default store;
