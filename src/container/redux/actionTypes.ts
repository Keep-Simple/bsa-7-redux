import {MessageState} from "../../types/types";

export const SEND_MESSAGE = 'SEND_MESSAGE';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const LIKE_MESSAGE = 'LIKE_MESSAGE';
export const TOGGLE_EDIT_WINDOW = 'TOGGLE_EDIT_WINDOW';
export const UP_ARROW_TRIGGERED = 'UP_ARROW_TRIGGERED';

interface SendMessageAction {
    type: typeof SEND_MESSAGE,
    payload: MessageState
}

interface DeleteMessageAction {
    type: typeof DELETE_MESSAGE
    payload: {
        id: string
    }
}

interface EditMessageAction {
    type: typeof EDIT_MESSAGE
    payload: {
        id: string,
        text: string
    }
}

interface ToggleEditWindowAction {
    type: typeof TOGGLE_EDIT_WINDOW
    payload: MessageState
}

interface LikeMessageAction {
    type: typeof LIKE_MESSAGE
    payload: {
        id: string
    }
}

interface UpArrowTriggeredAction {
    type: typeof UP_ARROW_TRIGGERED
}


export type ChatActionTypes = SendMessageAction | DeleteMessageAction |
    EditMessageAction | ToggleEditWindowAction |
    UpArrowTriggeredAction | LikeMessageAction;
