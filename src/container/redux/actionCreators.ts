import {
    ChatActionTypes,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    LIKE_MESSAGE,
    SEND_MESSAGE,
    TOGGLE_EDIT_WINDOW,
    UP_ARROW_TRIGGERED
} from "./actionTypes";
import {createMessageFromAdmin} from "../../mock/Admin";
import {MessageState} from "../../types/types";

export const sendMessage = (body: string): ChatActionTypes => ({
    type: SEND_MESSAGE,
    payload: createMessageFromAdmin(body)
});

export const deleteMessage = (ms: MessageState) => ({
    type: DELETE_MESSAGE,
    payload: {
        id: ms.id
    }
});

export const likeMessage = (ms: MessageState) => ({
    type: LIKE_MESSAGE,
    payload: {
        id: ms.id
    }
});

export const toggleEditWindow = (ms: MessageState) => ({
    type: TOGGLE_EDIT_WINDOW,
    payload: ms
});

export const upArrowKeyTriggered = () => ({
    type: UP_ARROW_TRIGGERED
});

export const editMessage = (ms: MessageState, body: string): ChatActionTypes => ({
    type: EDIT_MESSAGE,
    payload: {
        id: ms.id,
        text: body
    }
});
