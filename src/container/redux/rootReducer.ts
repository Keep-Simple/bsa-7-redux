import {
    ChatActionTypes,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    LIKE_MESSAGE,
    SEND_MESSAGE,
    TOGGLE_EDIT_WINDOW,
    UP_ARROW_TRIGGERED
} from "./actionTypes";
import {AppState} from "../../types/types";
import adminInfo from "../../mock/Admin";


export const rootReducer = (state: AppState = {} as AppState, action: ChatActionTypes): AppState => {
    switch (action.type) {
        case SEND_MESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.payload]
            };

        case DELETE_MESSAGE:
            return {
                ...state,
                messages: state.messages.filter(m => m.id !== action.payload.id)
            };

        case EDIT_MESSAGE:
            return {
                ...state,
                messages: state.messages.map(m => m.id === action.payload.id ?
                    {...m, text: action.payload.text, edit_date: String(new Date())} : m)
            };

        case TOGGLE_EDIT_WINDOW:
            return {
                ...state,
                editWindow: action.payload
            };

        case LIKE_MESSAGE:
            return {
                ...state,
                messages: state.messages.map(m => m.id === action.payload.id ?
                    m.isLiked ? {...m, isLiked: false} : {...m, isLiked: true} : m)
            };

        case UP_ARROW_TRIGGERED:
            const message = state.messages.slice(-1)[0];

            if (message.user_id === adminInfo.user_id && !state.editWindow.id)
                return {
                    ...state,
                    editWindow: message
                };

            return state;

        default:
            return state;
    }
}
