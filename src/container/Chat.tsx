import React, {useCallback, useEffect, useRef, useState} from 'react';
import Header from "../components/Header/Header";
import MessageInput from "../components/MessageInput/MessageInput";
import MessageList from "../components/MessageList/MessageList";
import {Dimmer, Loader} from "semantic-ui-react";
import {AppState} from "../types/types";
import {connect, ConnectedProps} from "react-redux";
import EditWindow from "../components/EditWindow/EditWindow";
import {toggleEditWindow, upArrowKeyTriggered} from "./redux/actionCreators";

const Chat = (props: ChatProps) => {

    const {messageCount, hasEditWindow, upArrow} = props;
    const [isLoading, setLoading] = useState(true);
    const endRef = useRef<HTMLDivElement>(null);

    const scrollToBottom = () => {
        if (endRef && endRef.current)
            endRef.current.scrollIntoView({behavior: "smooth"});
    }

    const handleKeyPress = useCallback(({key}: KeyboardEvent) =>
        key === "ArrowUp" && upArrow(), [upArrow]);

    useEffect(() => {
        setTimeout(() => scrollToBottom(), 50);
    }, [messageCount]);


    useEffect(() => {
        setTimeout(() => setLoading(false), 200);
    }, []);

    useEffect(() => {
        window.addEventListener("keydown", handleKeyPress);
        //remove on cleanup
        return () => window.removeEventListener('keydown', handleKeyPress);
    }, [handleKeyPress]);


    return (
        <>
            {isLoading &&
            <Dimmer active>
              <Loader/>
            </Dimmer>}
            <Header/>
            <MessageList/>
            <div ref={endRef}/>
            <MessageInput/>
            {hasEditWindow && <EditWindow/>}
        </>
    );
}


const mapState = (state: AppState) => ({
    messageCount: state.messages.length,
    hasEditWindow: Boolean(state.editWindow.id)
});

const mapDispatch = {
    editWindow: toggleEditWindow,
    upArrow: upArrowKeyTriggered
}

const connector = connect(mapState, mapDispatch);

type ChatProps = ConnectedProps<typeof connector>;

export default connector(Chat);
